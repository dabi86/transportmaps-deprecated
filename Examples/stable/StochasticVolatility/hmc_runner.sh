#!/bin/bash

DSET=DurbinData
DIST=All-50obs

EPS=0.1
NSTEPS=10

for i in `seq 50 51`;
do
    SMPS=$(($i*1000))
    echo "Samples $(($SMPS-1000))-${SMPS}"
    python ../../../scripts/tmap-postprocess \
      --data=data/${DSET}/Distributions/${DIST}/Lap-tol1e-4.dill \
      --output=data/${DSET}/Distributions/${DIST}/Lap-tol1e-4-POST.dill \
      --mcmc=hmc --mcmc-samples=${SMPS} --mcmc-hmc-eps=${EPS} --mcmc-hmc-nsteps=${NSTEPS} --log=20 -v --no-plotting -I
    # python ../../../scripts/tmap-postprocess \
    #   --data=data/DurbinData/Distributions/All-50obs/Seq-intsq-tot-poly-o1-q3-qn2-tol1e-4-hintsq-htot-hpoly-ho1-hq3-hqn2-htol1e-4.dill \
    #   --output=data/DurbinData/Distributions/All-50obs/Seq-intsq-tot-poly-o1-q3-qn2-tol1e-4-hintsq-htot-hpoly-ho1-hq3-hqn2-htol1e-4-POST.dill \
    #   --mcmc=hmc --mcmc-samples=${SMPS} --mcmc-hmc-eps=${EPS} --mcmc-hmc-nsteps=${NSTEPS} --log=20 -v --no-plotting
done
