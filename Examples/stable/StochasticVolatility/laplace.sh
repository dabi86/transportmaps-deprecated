#!/bin/bash

DSET=DurbinData
SIGMA=0.25
VAR_DIAG_QNUM=300000

N=945
while [ $N -gt 0 ]; do
    echo ""
    echo "Number of observations: $N"
    DIST=Sigma${SIGMA}-${N}obs
    mkdir -p data/${DSET}/Distributions/${DIST}
    if [ ! -f data/${DSET}/Distributions/${DIST}/Distribution.dill ]; then
        python DataGeneration.py \
            --output=data/${DSET}/Distributions/${DIST}/Distribution.dill \
            --input-data=data/${DSET}/Exchanges/Durbin.csv \
            --sigma=${SIGMA} -n ${N}
    fi
    if [ ! -f data/${DSET}/Distributions/${DIST}/Lap-tol1e-4.dill ]; then
        tmap-laplace \
            --dist=data/${DSET}/Distributions/${DIST}/Distribution.dill \
            --output=data/${DSET}/Distributions/${DIST}/Lap-tol1e-4.dill \
            --tol=1e-4 -f --log=20
    fi
    tmap-postprocess \
        --data=data/${DSET}/Distributions/${DIST}/Lap-tol1e-4.dill \
        --output=data/${DSET}/Distributions/${DIST}/Lap-tol1e-4-POST.dill \
        --var-diag=exact-base --var-diag-qtype=0 --var-diag-qnum=${VAR_DIAG_QNUM} -v
    # 2/3 number of observations
    N=$((N/4*3))
done
