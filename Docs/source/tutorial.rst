Tutorial
========

.. toctree::
   :maxdepth: 2

   statistical-models
   examples
   xml-descriptors.ipynb
   mpi-usage.ipynb
   cli/cli.ipynb
   linking
