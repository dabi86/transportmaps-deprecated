#!/bin/bash

set -e

ROOT=$(pwd)
DOCS_ROOT=Docs

cd "$DOCS_ROOT"

echo ""
echo "Make"
cd make3
./build-api
make clean
make html
cd ..

echo ""
echo "Entering maintenance mode"
CMD="cd /mit/uqlab/web_scripts/transportmaps-website/;"
CMD+="mv .htaccess .htaccess.bak"
ssh athena.dialup.mit.edu "$CMD"

echo ""
echo "Backing up former version"
CMD="cd /mit/uqlab/web_scripts/transportmaps-website/;"
CMD+="mv docs docs-bak;"
CMD+="mkdir docs;"
CMD+="tar -czf docs-bak.tar.gz docs-bak --remove-files"
ssh athena.dialup.mit.edu "$CMD"

echo ""
echo "Upload"
scp -r build3/html/* dabi@athena.dialup.mit.edu:/mit/uqlab/web_scripts/transportmaps-website/docs/

echo ""
echo "Exiting maintenance mode"
CMD="cd /mit/uqlab/web_scripts/transportmaps-website/;"
CMD+="mv .htaccess.bak .htaccess"
ssh athena.dialup.mit.edu "$CMD"
