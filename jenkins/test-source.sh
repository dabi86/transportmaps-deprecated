#!/bin/bash

set -e

ROOT=$(pwd)
TEST_NAME=$(echo "$JOB_NAME"| cut -d / -f 1)
ENV_NAME=venv_${TEST_NAME}_${PYTHON_EXE}
VENV_OPTS=""

if [[ "$TEST_NAME" == *"mpi"* ]]; then
    TTYPE='parallel'
    MPI=True
else
    TTYPE='serial'
    MPI=False
fi

if [[ `hostname` == *"macys"* ]]; then
    # VENV_OPTS="$VENV_OPTS --system-site-packages"
    export PATH=$PATH:/usr/local/bin
    if [[ $PYTHON_EXE == "python2" ]]; then
        PYTHON_EXE=python
    fi
fi

if [[ `hostname` == "macys-Sierra" ]]; then
    export LC_ALL=en_US.UTF-8
    export LANG=en_US.UTF-8
    # MPI=False
fi

if [[ `hostname` == *"reynolds"* ]]; then
    export PATH=$PATH:/home/jenkins/bin
fi

if [ "$VENV_SYS" = "virtualenv" ]; then
    rm -rf $ENV_NAME
    virtualenv $VENV_OPTS --python=$PYTHON_EXE $ENV_NAME
    source $ENV_NAME/bin/activate
# elif [ "$VENV_SYS" = "anaconda" ]; then
#     $CONDA_HOME/bin/conda-env remove -y --name $ENV_NAME
#     if [ "$PYTHON_EXE" = "python2" ]; then
#         $CONDA_HOME/bin/conda create -y --name $ENV_NAME python=2 pip
#     elif [ "$PYTHON_EXE" = "python3" ]; then
#         $CONDA_HOME/bin/conda create -y --name $ENV_NAME python=3 pip
#     fi
#     source $CONDA_HOME/bin/activate $ENV_NAME
else
    echo "VENV_SYS not recognized"
    exit 1
fi

PY_VERS=$(python -V)
# Python test command
CMD="import logging;"
CMD+="import TransportMaps as TM;"
CMD+="import warnings;"
CMD+="warnings.simplefilter('ignore');"
CMD+="TM.tests.run_all(log_level=logging.ERROR,ttype='$TTYPE')"

echo ""
echo "TEST_NAME: $TEST_NAME"
echo "HOSTNAME: $(hostname)"
echo "PATH: $PATH"
echo "VENV_OPTS: $VENV_OPTS"
echo "PY-VERS: $PY_VERS"
echo "PY-CMD: $CMD"
echo ""

echo "Upgrading pip"
python $ROOT/$ENV_NAME/bin/pip install --upgrade pip

echo "Installing numpy"
python $ROOT/$ENV_NAME/bin/pip install --upgrade numpy

# Install
echo "Packaging"
rm -fr tmp_sdist
mkdir tmp_sdist
python setup.py clean --all
python setup.py sdist --dist-dir=tmp_sdist
cd tmp_sdist

# Install TransportMaps
echo "Installing"
INSTALLER=$(ls)
PIP_FLAGS=--no-cache-dir CC=$COMPILER MPI="$MPI" H5PY=True python $ROOT/$ENV_NAME/bin/pip install --upgrade $INSTALLER
cd -

# Unit tests
echo "Running tests"
rm -fr tmp_run
mkdir tmp_run
cd tmp_run
python -c "$CMD"
cd -

# Removing
# cd ../
# rm -rf tmp_sdist

if [ "$VENV_SYS" = "virtualenv" ]; then
    deactivate
    # rm -rf $ENV_DIR/$ENV_NAME
# elif [ "$VENV_SYS" = "anaconda" ]; then
#     source $CONDA_HOME/bin/deactivate
#     $CONDA_HOME/bin/conda-env remove -y --name $ENV_NAME
fi
