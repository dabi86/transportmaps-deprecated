#!/bin/bash

rm -rf dist TransportMaps.egg-info build
find . -type f -name '*~' -delete
python setup.py sdist
twine upload dist/* 
